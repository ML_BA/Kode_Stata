# Information om koden

Koden til Mathilde Larsens bachelorprojekt i Økonomi på Københavns Universitet kan tilgås her.

Størstedelen af databehandling og samtlige regressioner er lavet i Stata. Koden i filen 'Stata_Kode' skal derfor indsættes i en DO-fil for at kunne køre, og kræver det nødvendige data.
Koden er derfor kun lagt ind her af oplysende årsager.

Såfremt man vil forsøge at genskabe resultaterne, kan man kontakte Mathilde Larsen på cdv922@alumni.ku.dk for det tilhørende data.
